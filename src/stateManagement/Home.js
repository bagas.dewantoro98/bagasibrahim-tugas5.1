import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

const Home = () => {
  const {isLoggedIn, userData} = useSelector(state => state.auth);
  console.log('cek userData', userData);

  return (
    <View>
      <Text>Home</Text>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
